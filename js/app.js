'use strict';

var app = angular.module('HsvSpawns', []);

app.controller('HsvSpawnsCtrl', ['$scope', '$http', HsvSpawnsCtrl]);

function HsvSpawnsCtrl($scope, $http) {
  var map = L.map('map', 'mapbox.streets').setView([34.7304, -86.5861], 13);
  var heat;

  $scope.pokemon = ["All", "Bulbasaur", "Ivysaur", "Venusaur", "Charmander", "Charmeleon", "Charizard", "Squirtle", "Wartortle", "Blastoise", "Caterpie", "Metapod", "Butterfree", "Weedle", "Kakuna", "Beedrill", "Pidgey", "Pidgeotto", "Pidgeot", "Rattata", "Raticate", "Spearow", "Fearow", "Ekans", "Arbok", "Pikachu", "Raichu", "Sandshrew", "Sandslash", "Nidoranâ™€", "Nidorina", "Nidoqueen", "Nidoranâ™‚", "Nidorino", "Nidoking", "Clefairy", "Clefable", "Vulpix", "Ninetales", "Jigglypuff", "Wigglytuff", "Zubat", "Golbat", "Oddish", "Gloom", "Vileplume", "Paras", "Parasect", "Venonat", "Venomoth", "Diglett", "Dugtrio", "Meowth", "Persian", "Psyduck", "Golduck", "Mankey", "Primeape", "Growlithe", "Arcanine", "Poliwag", "Poliwhirl", "Poliwrath", "Abra", "Kadabra", "Alakazam", "Machop", "Machoke", "Machamp", "Bellsprout", "Weepinbell", "Victreebel", "Tentacool", "Tentacruel", "Geodude", "Graveler", "Golem", "Ponyta", "Rapidash", "Slowpoke", "Slowbro", "Magnemite", "Magneton", "Farfetch'd", "Doduo", "Dodrio", "Seel", "Dewgong", "Grimer", "Muk", "Shellder", "Cloyster", "Gastly", "Haunter", "Gengar", "Onix", "Drowzee", "Hypno", "Krabby", "Kingler", "Voltorb", "Electrode", "Exeggcute", "Exeggutor", "Cubone", "Marowak", "Hitmonlee", "Hitmonchan", "Lickitung", "Koffing", "Weezing", "Rhyhorn", "Rhydon", "Chansey", "Tangela", "Kangaskhan", "Horsea", "Seadra", "Goldeen", "Seaking", "Staryu", "Starmie", "Mr. Mime", "Scyther", "Jynx", "Electabuzz", "Magmar", "Pinsir", "Tauros", "Magikarp", "Gyarados", "Lapras", "Ditto", "Eevee", "Vaporeon", "Jolteon", "Flareon", "Porygon", "Omanyte", "Omastar", "Kabuto", "Kabutops", "Aerodactyl", "Snorlax", "Articuno", "Zapdos", "Moltres", "Dratini", "Dragonair", "Dragonite", "Mewtwo", "Mew"];

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiZGFudGhpbmdvIiwiYSI6ImNpcmM0czJ0bjAxYzRnNG05M2NtcWEzZmUifQ.AipgL7fB_gporwkzmGSe8g'
  }).addTo(map);

  $scope.form = {
    pokemon: "All"
  };

  heat = L.heatLayer([], {radius: 25}).addTo(map);

  $scope.generate = function () {
    var index = $scope.pokemon.indexOf($scope.form.pokemon);
    if (index == 0) {
      index = '';
    } else {
      index = index.toString();
    }

    $http.get('/hsvspawns/api/spawns/' + index).success(function(spawns) {
        heat.setLatLngs(spawns);
        heat.setOptions({radius: 25});
    });
  }

  $scope.generate();
}

app.controller('HsvNodesCtrl', ['$scope', '$http', HsvNodesCtrl]);

function HsvNodesCtrl($scope, $http) {
  var pokemonNames = ["All", "Bulbasaur", "Ivysaur", "Venusaur", "Charmander", "Charmeleon", "Charizard", "Squirtle", "Wartortle", "Blastoise", "Caterpie", "Metapod", "Butterfree", "Weedle", "Kakuna", "Beedrill", "Pidgey", "Pidgeotto", "Pidgeot", "Rattata", "Raticate", "Spearow", "Fearow", "Ekans", "Arbok", "Pikachu", "Raichu", "Sandshrew", "Sandslash", "Nidoran♀", "Nidorina", "Nidoqueen", "Nidoran♂", "Nidorino", "Nidoking", "Clefairy", "Clefable", "Vulpix", "Ninetales", "Jigglypuff", "Wigglytuff", "Zubat", "Golbat", "Oddish", "Gloom", "Vileplume", "Paras", "Parasect", "Venonat", "Venomoth", "Diglett", "Dugtrio", "Meowth", "Persian", "Psyduck", "Golduck", "Mankey", "Primeape", "Growlithe", "Arcanine", "Poliwag", "Poliwhirl", "Poliwrath", "Abra", "Kadabra", "Alakazam", "Machop", "Machoke", "Machamp", "Bellsprout", "Weepinbell", "Victreebel", "Tentacool", "Tentacruel", "Geodude", "Graveler", "Golem", "Ponyta", "Rapidash", "Slowpoke", "Slowbro", "Magnemite", "Magneton", "Farfetch'd", "Doduo", "Dodrio", "Seel", "Dewgong", "Grimer", "Muk", "Shellder", "Cloyster", "Gastly", "Haunter", "Gengar", "Onix", "Drowzee", "Hypno", "Krabby", "Kingler", "Voltorb", "Electrode", "Exeggcute", "Exeggutor", "Cubone", "Marowak", "Hitmonlee", "Hitmonchan", "Lickitung", "Koffing", "Weezing", "Rhyhorn", "Rhydon", "Chansey", "Tangela", "Kangaskhan", "Horsea", "Seadra", "Goldeen", "Seaking", "Staryu", "Starmie", "Mr. Mime", "Scyther", "Jynx", "Electabuzz", "Magmar", "Pinsir", "Tauros", "Magikarp", "Gyarados", "Lapras", "Ditto", "Eevee", "Vaporeon", "Jolteon", "Flareon", "Porygon", "Omanyte", "Omastar", "Kabuto", "Kabutops", "Aerodactyl", "Snorlax", "Articuno", "Zapdos", "Moltres", "Dratini", "Dragonair", "Dragonite", "Mewtwo", "Mew"];

  var map = L.map('map', 'mapbox.streets').setView([34.7304, -86.5861], 13);
  var nodes = [];
  var markers = [];

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiZGFudGhpbmdvIiwiYSI6ImNpcmM0czJ0bjAxYzRnNG05M2NtcWEzZmUifQ.AipgL7fB_gporwkzmGSe8g'
  }).addTo(map);

  function generateSpawnTable(node) {
    var table = '<table class="table table-condensed table-striped table-bordered">';

    angular.forEach(node.pokemon, function (pokemon) {
      table += '<tr><td>' + pokemonNames[pokemon[0]] + '</td><td>' + pokemon[1] + '</td><td>' + (pokemon[1]/node.total_spawns*100).toFixed(2) + '%</td></tr>';
    });

    table += '<tr><td><b>Total</b></td><td>' + node.total_spawns + '</td><td></td></tr>';
    table += '<tr><td><b>Minute</b></td><td>' + node.minute + '</td><td></td></tr>';

    table += '</table>';

    return table;
  }

  function fetchNodes(coords) {
    $http.get('/hsvspawns/api/nodes/' + coords.lat + '/' + coords.lng).success(function(nodes) {
      angular.forEach(nodes, function (node) {
        var marker = L.marker([node.latitude, node.longitude]);
        marker.bindPopup(generateSpawnTable(node));
        marker.addTo(map);
        markers.push(marker);
      });
    });
  }

  var coords = {
    lat: 34.7304,
    lng: -86.5861
  };

  map.on('click', function (e) {
    coords = e.latlng;
  });

  fetchNodes(coords);

  $scope.scan = function () {
    fetchNodes(coords);
  };
}
