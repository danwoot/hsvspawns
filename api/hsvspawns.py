from flask import Flask, jsonify, Response, g
from sqlite3 import dbapi2 as sqlite3
import json
from werkzeug.routing import FloatConverter as BaseFloatConverter


class FloatConverter(BaseFloatConverter):
	regex = r'-?\d+(\.\d+)?'

DATABASE = 'pokemon.db'
app = Flask(__name__)

app.url_map.converters['float'] = FloatConverter

def get_db():
	db = getattr(g, '_database', None)
	if db is None:
		db = g._database = sqlite3.connect(DATABASE)
	return db

@app.teardown_appcontext
def close_connection(exception):
	db = getattr(g, '_database', None)
	if db is not None:
		db.close()

def run_sql(sql):
	db = get_db()
        rv = db.execute(sql)
        res = rv.fetchall()
        rv.close()
        return res

def select_spawns():
	sql = "select latitude, longitude, 0.5 from spawns"
	return run_sql(sql)

def select_pokemon_spawns(pokemon_id):
	sql = "SELECT pokemon_spawns.latitude, pokemon_spawns.longitude, pokemon_spawns.count*10.0/pokemon_spawns_unique.count FROM pokemon_spawns JOIN pokemon_spawns_unique ON pokemon_spawns.spawnpoint_id=pokemon_spawns_unique.spawnpoint_id WHERE pokemon_id = " + str(pokemon_id)
	return run_sql(sql)

def select_nodes(latitude, longitude):
	WINDOW_SIZE = 0.001
	sql = "SELECT * FROM node WHERE latitude < " + str(latitude + WINDOW_SIZE) + " AND latitude > " + str(latitude - WINDOW_SIZE) + " AND longitude < " + str(longitude + WINDOW_SIZE) + " AND longitude > " + str(longitude - WINDOW_SIZE) + " ORDER BY minute"
	return run_sql(sql)

def select_node_pokemon(node_id):
	sql = "SELECT pokemon_id, spawn_count FROM node_pokemon WHERE node_id='" + node_id + "' ORDER BY spawn_count DESC"
	return run_sql(sql)

@app.route('/')
def index():
	return 'Hello World!'

@app.route('/spawns/', defaults={'pokemon_id': None})
@app.route('/spawns/<int:pokemon_id>')
def get_spawns(pokemon_id):
	if pokemon_id is None:
		return Response(json.dumps(select_spawns()), mimetype='application/json')
	else:
		return Response(json.dumps(select_pokemon_spawns(pokemon_id)), mimetype='application/json')

@app.route('/nodes/<float:latitude>/<float:longitude>')
def get_nodes(latitude, longitude):
	raw_nodes = select_nodes(latitude, longitude)
	nodes = []

	for raw_node in raw_nodes:
		node = {
			'id': raw_node[0],
			'minute': raw_node[1],
			'latitude': raw_node[2],
			'longitude': raw_node[3],
			'total_spawns': raw_node[4],
			'pokemon': select_node_pokemon(raw_node[0]) 
		}
		nodes.append(node)	
		
	return Response(json.dumps(nodes), mimetype='application/json')
