from sqlite3 import dbapi2 as sqlite3

db = sqlite3.connect('pokemon.db')

def fetchSpawns():
	cursor = db.cursor()
	spawns = cursor.execute('SELECT spawnpoint_id FROM pokemon_spawns_unique')
	return spawns.fetchall()

def saveCount(spawnpoint_id, count):
	cursor = db.cursor()
        cursor.execute("UPDATE pokemon_spawns_unique SET count=" + str(count) + " WHERE spawnpoint_id='" + spawnpoint_id + "'")
	db.commit()

def countSpawns(spawnpoint_id):
	cursor = db.cursor()
	pokemon = cursor.execute("SELECT count FROM pokemon_spawns WHERE spawnpoint_id='" + spawnpoint_id + "'")
	rows = pokemon.fetchall()

	count = 0
	for row in rows:
		count += row[0]

	saveCount(spawnpoint_id, count)


rows = fetchSpawns()

for row in rows:
	countSpawns(row[0])
	
